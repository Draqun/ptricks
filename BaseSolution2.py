class BaseMeta(type):
    def __new__(cls, name, bases, body):
        print(f"cls={cls},\nname={name},\nbases={bases},\nbody={body}")
        obj = super().__new__(cls, name, bases, body)
        print(type(body))
        if name != "Base" and "bis" not in body:
            raise TypeError(f'{name} class has no attr bis!')
        return obj


class Base(metaclass=BaseMeta):
    def foo(self):
        return "foo"

    def bar(self):
        return self.bis()
