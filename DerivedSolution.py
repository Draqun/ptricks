from Base import Base

assert hasattr(Base, "foo"), "foo attribute disappeared from Base class"


class Derived(Base):
    def bis(self):
        return self.foo()
