class Base():
    def foo(self):
        return self.bis()

    def __init_subclass__(cls, scm_type=None, name=None, **kwargs):
        obj = super().__init_subclass__(**kwargs)
        if not hasattr(cls, "bis"):
            raise TypeError(f'{cls.__name__} class has no attr bis!')
        return obj