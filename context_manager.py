with open("Base.py") as file:
    text = file.read()

print(text)

class B:
    def __enter__(self):
        print("Enter")

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("Exit")

with B() as b:
    pass
