class Base:
    def foo(self):
        return "foo"

    def bar(self):
        return self.bis()


__old_bd = __build_class__


def my_bc(func, name, *args, **kwargs):
    print("func={}, name={}, args={}, kwargs={}".format(func, name, args, kwargs))

    obj = __old_bd(func, name, *args, **kwargs)

    assert hasattr(obj, "bis"), f'{name} class has no attr bis!'
    return obj


import builtins
builtins.__build_class__ = my_bc
