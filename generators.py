from time import sleep

def fun():
    for a in range(10):
        yield a
        sleep(.5)


class Generator():
    def __iter__(self):
        self.num = 0
        return self

    def __next__(self):
        if self.num > 10:
            raise StopIteration()

        sleep(.5)
        self.num = self.num + 1
        return self.num

# for el in Generator():
#     print(el)

def d(t):
    return [b for b in t if b % 7]

a = (el for el in range(100) if el % 2)
for ab in a:
    print(ab)
print(type(a))

class Api():
    def __run_first(self):
        pass

    def __run_second(self):
        pass

    def __run_third(self):
        pass

    def run(self, lmbd, *args, **kwargs):
        self.__run_first()
        lmbd
        self.__run_second()
        yield
        self.__run_third()
